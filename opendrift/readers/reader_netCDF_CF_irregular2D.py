# This file is part of OpenDrift.
#
# OpenDrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 2
#
# OpenDrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with OpenDrift.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2015, Knut-Frode Dagestad, MET Norway

from bisect import bisect_left, bisect_right
from datetime import datetime
import logging
import os
import pickle
logger = logging.getLogger(__name__)

import numpy as np
import netCDF4 as nc
from netCDF4 import num2date
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator
import xarray as xr

from opendrift.readers.basereader import BaseReader, vector_pairs_xy, StructuredReader
from opendrift.readers.roppy import depth

default_variable_mapping = {'nav_lon': 'longitude',
                            'nav_lat': 'latitude',
                            'time_counter': 'time',
                            'u_wind': 'x_wind',
                            'v_wind': 'y_wind'}

class Reader(BaseReader, StructuredReader):

    def __init__(self, filename=None, name=None,
                 variable_mapping=None,
                 time_dim='time_counter',
                 geobuffer=0.2,
                 lon_var='nav_lon',
                 lat_var='nav_lat'):

        if filename is None:
            raise ValueError('Need filename as argument to constructor')
        
        self.geobuffer=geobuffer
        self.proj4='+proj=latlong'
        # Map variable names to CF standard_name
        if variable_mapping is None:
            self.variable_mapping = default_variable_mapping
        else:
            self.variable_mapping = variable_mapping
        filestr = str(filename)
        try:
            # Open file, check that everything is ok
            logger.info('Opening dataset: ' + filestr)
            if ('*' in filestr) or ('?' in filestr) or ('[' in filestr):
                logger.info('Opening files with MFDataset')
                self.Dataset = xr.open_mfdataset(
                    filename,
                    chunks={time_dim: 1}, compat='override',
                    decode_times=False,
                    data_vars='minimal', coords='minimal')
            else:
                logger.info('Opening file with Dataset')
                self.Dataset = xr.open_dataset(filename, decode_times=False)
        except Exception as e:
            raise ValueError(e)

        for var in list(self.variable_mapping):  # Remove unused variables
            if var not in self.Dataset.variables:
                del self.variable_mapping[var]

        # Grid stuff
        lon = self.Dataset[lon_var].values[:]
        lat = self.Dataset[lat_var].values[:]
        if lon.ndim == 1:
            lon, lat = np.meshgrid(lon, lat)
        self.lat = lat
        self.lon = lon
        self.lon[self.lon>180] = self.lon[self.lon>180] - 360
        self.xmin=np.min(self.lon)
        self.xmax=np.max(self.lon)
        self.ymin=np.min(self.lat)
        self.ymax=np.max(self.lat)
        delta_lon=np.diff(self.lon,axis=-1)
        delta_lat=np.diff(self.lat,axis=0)
        self.delta_x=np.abs(np.mean(delta_lon))
        self.delta_y=np.abs(np.mean(delta_lat))
        self.shape = (int(self.lon.shape[-1]), int( self.lon.shape[0]))
        self.corners = [[self.lon[-1,0], self.lon[0, 0], self.lon[-1, -1], self.lon[0,-1]],
                        [self.lat[-1,0], self.lat[0, 0], self.lat[-1, -1], self.lat[0,-1]]]

        # Get time coverage
        try:
            time_counter = self.Dataset.variables['time_counter']
        except:
            time_counter = self.Dataset.variables['time']
        time_units = time_counter.attrs['units']
        if time_units == 'second':
            logger.info('Ocean time given as seconds relative to start '
                         'Setting artifical start time of 1 Jan 2000.')
            time_units = 'seconds since 2000-01-01 00:00:00'
        self.times = num2date(time_counter[:], time_units)
        self.start_time = self.times[0]
        self.end_time = self.times[-1]
        if len(self.times) > 1:
            self.time_step = self.times[1] - self.times[0]
        else:
            self.time_step = None

        if name is None:
            self.name = filestr
        else:
            self.name = name

        # Find all variables having standard_name
        self.variables = []
        for var_name in self.Dataset.variables:
            if var_name in self.variable_mapping.keys():
                var = self.Dataset.variables[var_name]
                self.variables.append(self.variable_mapping[var_name])
        # Run constructor of parent Reader class
        super(Reader, self).__init__()
        

    def get_variables(self, requested_variables, time=None,
                      x=None, y=None, z=None):
        start_time = datetime.now()
        requested_variables, time, x, y, z, outside = self.check_arguments(
            requested_variables, time, x, y, z)
        # If one vector component is requested, but not the other
        # we must add the other for correct rotation
        for vector_pair in vector_pairs_xy:
            if (vector_pair[0] in requested_variables and
                vector_pair[1] not in requested_variables):
                requested_variables.extend([vector_pair[1]])
            if (vector_pair[1] in requested_variables and
                vector_pair[0] not in requested_variables):
                requested_variables.extend([vector_pair[0]])

        nearestTime, dummy1, dummy2, indxTime, dummy3, dummy4 = \
            self.nearest_time(time)

        x = np.atleast_1d(x)
        y = np.atleast_1d(y)

        # Indentify indices that cover requested positions
        lonmin = x.min() - self.geobuffer
        lonmax = x.max() + self.geobuffer
        latmin = y.min() - self.geobuffer
        latmax = y.max() + self.geobuffer
        lon_flat = self.lon.flatten()
        lat_flat = self.lat.flatten()
        inds = np.where((lon_flat > lonmin) &
                        (lon_flat < lonmax) &
                        (lat_flat > latmin) &
                        (lat_flat < latmax))[0]

        # Generate interpolation grid
        lonsm = np.arange(lonmin, lonmax, self.delta_x)
        latsm = np.arange(latmin, latmax, self.delta_y)
        lonsm, latsm = np.meshgrid(lonsm, latsm)
        
        # Initializing dictionary to contain data
        variables = {'z': z,
                    'time': nearestTime}

        for par in requested_variables:
            if par == 'land_binary_mask':
                variables[par] = np.asarray(self.land_binary_mask).flatten()[inds]
            else:
                varname = [name for name, cf in
                           self.variable_mapping.items() if cf == par]
                var = self.Dataset.variables[varname[0]].squeeze()
                if var.ndim == 2:
                    variables[par] = np.asarray(var).flatten()[inds]
                elif var.ndim == 3:
                    variables[par] = np.asarray(var[indxTime, ...]).flatten()[inds]
                else:
                    raise Exception('Wrong dimension of variable: ' +
                                    self.variable_mapping[par])

            # Mask values outside domain
            variables[par] = np.ma.array(variables[par], mask=False)

        variables['x'] = lonsm[0, :]
        variables['y'] = latsm[:, 0]

        variables['x'] = variables['x'].astype(np.float32)
        variables['y'] = variables['y'].astype(np.float32)
        variables['time'] = nearestTime

        # Finally, interpolate to a regular lat/lon grid
        for var in requested_variables:
            lono = lon_flat[inds]
            lato = lat_flat[inds]
            logger.debug('Making interpolator...')
            if var == 'land_binary_mask':
                interpolator = NearestNDInterpolator((lato,
                                                      lono),
                                                      variables[var])
                variables[var] = interpolator(latsm, lonsm)
            else:
                interpolator = LinearNDInterpolator((lato,
                                                     lono),
                                                    variables[var])
                variables[var] = interpolator(latsm, lonsm)

            variables[var] = np.ma.masked_invalid(variables[var])
        logger.debug('Time for netCDF_CF_irregular2D reader: ' + str(datetime.now()-start_time))
        return variables


