# This file is part of OpenDrift.
#
# OpenDrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 2
#
# OpenDrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with OpenDrift.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2015, Knut-Frode Dagestad, MET Norway

from datetime import datetime
import logging
import os
import pickle
logger = logging.getLogger(__name__)

import numpy as np
import netCDF4 as nc
from netCDF4 import num2date
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator
import xarray as xr

from opendrift.readers.basereader import BaseReader, vector_pairs_xy, StructuredReader


NEMO_variable_mapping = {'vozocrtx': 'x_sea_water_velocity',
                         'vomecrty': 'y_sea_water_velocity',
                         'time_counter': 'time',
                         'nav_lon': 'longitude',
                         'nav_lat': 'latitude',
}

class Reader(BaseReader, StructuredReader):

    def __init__(self, filename=None, name=None, meshfile=None,
                 variable_mapping=None, grid_type='cgrid', orca_grid=False,
                 time_dim='time_counter',
                 rotation_pickle_file=None, model_land_mask=True,
                 geobuffer=0.2):

        if filename is None:
            raise ValueError('Need filename as argument to constructor')
        if meshfile is None:
            raise ValueError('Need meshfile as argument to constructor')
        
        self.geobuffer=geobuffer
        self.grid_type = grid_type
        self.orca_grid = orca_grid
        self.proj4='+proj=latlong'
        # Map variable names to CF standard_name
        if variable_mapping is None:
            self.variable_mapping = NEMO_variable_mapping
        else:
            self.variable_mapping = variable_mapping
        filestr = str(filename)
        try:
            # Open file, check that everything is ok
            logger.info('Opening dataset: ' + filestr)
            if ('*' in filestr) or ('?' in filestr) or ('[' in filestr):
                logger.info('Opening files with MFDataset')
                self.Dataset = xr.open_mfdataset(
                    filename,
                    chunks={time_dim: 1}, compat='override',
                    decode_times=False,
                    data_vars='minimal', coords='minimal')
            else:
                logger.info('Opening file with Dataset')
                self.Dataset = xr.open_dataset(filename, decode_times=False)
        except Exception as e:
            raise ValueError(e)

        for var in list(self.variable_mapping):  # Remove unused variables
            if var not in self.Dataset.variables:
                del self.variable_mapping[var]

        # Grid stuff 
        with xr.open_dataset(meshfile) as gf:
            self.lat = gf.variables['nav_lat'].values[:]
            self.lon = gf.variables['nav_lon'].values[:]
            self.lat_U = gf.variables['gphiu'].values[0]
            self.lon_U = gf.variables['glamu'].values[0]
            self.lat_V = gf.variables['gphiv'].values[0]
            self.lon_V = gf.variables['glamv'].values[0]

            # Calculate the bathymetry
            mbathy = gf.variables['mbathy'][0]
            gdepw = gf.variables['gdepw_0'][0]
            tmask = gf.variables['tmask'][0]
            bathy = gdepw[mbathy]*tmask
            self.z = gf.variables['gdept_1d'].values[0]
            self.sea_floor_depth_below_sea_level = bathy.values[:]
            # Generate the land binary mask from tmask at surface
            if tmask.ndim == 3:
                tmask = tmask[0]
            land_binary_mask = 1.0 - tmask
            self.land_binary_mask = land_binary_mask
            self.Dataset = self.Dataset.assign({'land_binary_mask':
                                                land_binary_mask})
        self.xmin=np.min(self.lon)
        self.xmax=np.max(self.lon)
        self.ymin=np.min(self.lat)
        self.ymax=np.max(self.lat)
        delta_lon=np.diff(self.lon,axis=-1)
        delta_lat=np.diff(self.lat,axis=0)
        self.delta_x=np.abs(np.mean(delta_lon))
        self.delta_y=np.abs(np.mean(delta_lat))
        self.shape = (int(self.lon.shape[-1]), int( self.lon.shape[0]))
        self.corners = [[self.lon[-1,0], self.lon[0, 0], self.lon[-1, -1], self.lon[0,-1]],
                        [self.lat[-1,0], self.lat[0, 0], self.lat[-1, -1], self.lat[0,-1]]]
        # Caclulate rotation angles
        if rotation_pickle_file:
            if os.path.exists(rotation_pickle_file):
                with open(rotation_pickle_file,'rb') as f:
                    coeffs = pickle.load(f)
            else:
                coeffs = opa_angle_2016_p(meshfile, orca_grid=self.orca_grid)
                with open(rotation_pickle_file,'wb',-1) as f:
                    pickle.dump(coeffs, f)
        else:
            coeffs = opa_angle_2016_p(meshfile, orca_grid=self.orca_grid)
        self.angles = coeffs

        # Get time coverage
        try:
            time_counter = self.Dataset.variables['time_counter']
        except:
            time_counter = self.Dataset.variables['time']
        time_units = time_counter.attrs['units']
        if time_units == 'second':
            logger.info('Ocean time given as seconds relative to start '
                         'Setting artifical start time of 1 Jan 2000.')
            time_units = 'seconds since 2000-01-01 00:00:00'
        self.times = num2date(time_counter[:], time_units)
        self.start_time = self.times[0]
        self.end_time = self.times[-1]
        if len(self.times) > 1:
            self.time_step = self.times[1] - self.times[0]
        else:
            self.time_step = None

        if name is None:
            self.name = 'NEMO native'
        else:
            self.name = name

        # Find all variables having standard_name
        self.variables = []
        for var_name in self.Dataset.variables:
            if var_name in self.variable_mapping.keys():
                var = self.Dataset.variables[var_name]
                self.variables.append(self.variable_mapping[var_name])
        if model_land_mask:
            self.variables.append('land_binary_mask')
        # Run constructor of parent Reader class
        super(Reader, self).__init__()
        

    def get_variables(self, requested_variables, time=None,
                      x=None, y=None, z=None):
        start_time = datetime.now()
        requested_variables, time, x, y, z, outside = self.check_arguments(
            requested_variables, time, x, y, z)
        # If one vector component is requested, but not the other
        # we must add the other for correct rotation
        for vector_pair in vector_pairs_xy:
            if (vector_pair[0] in requested_variables and
                vector_pair[1] not in requested_variables):
                requested_variables.extend([vector_pair[1]])
            if (vector_pair[1] in requested_variables and
                vector_pair[0] not in requested_variables):
                requested_variables.extend([vector_pair[0]])

        nearestTime, dummy1, dummy2, indxTime, dummy3, dummy4 = \
            self.nearest_time(time)

        variables = {}

        if z is None:
            z = np.atleast_1d(0)

        # Indentify indices that cover requested positions
        lonmin = x.min() - self.geobuffer
        lonmax = x.max() + self.geobuffer
        latmin = y.min() - self.geobuffer
        latmax = y.max() + self.geobuffer
        lon_flat = self.lon.flatten()
        lat_flat = self.lat.flatten()
        inds = np.where((lon_flat > lonmin) &
                        (lon_flat < lonmax) &
                        (lat_flat > latmin) &
                        (lat_flat < latmax))[0]

        # Generate interpolation grid
        lonsm = np.arange(lonmin, lonmax, self.delta_x)
        latsm = np.arange(latmin, latmax, self.delta_y)
        lonsm, latsm = np.meshgrid(lonsm, latsm)
        
        # Find depth levels covering all elements
        if hasattr(self, 'z') and (z is not None):
            # Find z-index range
            # NB: may need to flip if self.z is ascending
            indices = np.searchsorted(self.z, [z.min(), z.max()])
            indz = np.arange(np.maximum(0, indices.min() - 1 -
                                        self.verticalbuffer),
                             np.minimum(len(self.z), indices.max() + 1 +
                                        self.verticalbuffer))
            if len(indz) == 1:
                indz = indz[0]  # Extract integer to read only one layer
        else:
            indz = 0
        variables['z'] = np.array(self.z[indz]) 

        for par in requested_variables:

            if par == 'land_binary_mask':
                variables[par] = np.asarray(self.land_binary_mask).flatten()[inds]
            else:
                varname = [name for name, cf in
                           self.variable_mapping.items() if cf == par]
                var = self.Dataset.variables[varname[0]].squeeze()
                if var.ndim == 2:
                    variables[par] = np.asarray(var).flatten()[inds]
                elif var.ndim == 3:
                    variables[par] = np.asarray(var[indxTime, ...]).flatten()[inds]
                elif var.ndim == 4:                    
                    vnew=np.empty((len(indz),len(inds)))
                    for n, iz, in enumerate(indz):
                        vnew[n,:] = np.asarray(var[indxTime, iz, :]).flatten()[inds]
                    variables[par] = vnew

                else:
                    raise Exception('Wrong dimension of variable: ' +
                                    self.variable_mapping[par])

            start = datetime.now()

            # Mask values outside domain
            variables[par] = np.ma.array(variables[par], mask=False)

        variables['x'] = lonsm[0, :]
        variables['y'] = latsm[:, 0]

        variables['x'] = variables['x'].astype(np.float32)
        variables['y'] = variables['y'].astype(np.float32)
        variables['time'] = nearestTime

        if 'x_sea_water_velocity' in variables.keys():
            # We must rotate current vectors
            coeffs = [np.asarray(c).flatten()[inds] for c in self.angles]
            if 'x_sea_water_velocity' in variables.keys():
                # Temporary holders for u/v data
                utmp = variables['x_sea_water_velocity']
                vtmp = variables['y_sea_water_velocity']
                if self.grid_type =='cgrid':
                    variables['x_sea_water_velocity'] = rot_rep_2017_p(
                        utmp,
                        vtmp,
                        'U', 'ij->e', coeffs)
                    variables['y_sea_water_velocity'] = rot_rep_2017_p(
                        utmp,
                        vtmp,
                        'V', 'ij->n', coeffs)


                elif self.grid_type == 'agrid':
                    variables['x_sea_water_velocity'] = rot_rep_2017_p(
                        utmp,
                        vtmp,
                        'T', 'ij->e', coeffs)
                    variables['y_sea_water_velocity'] = rot_rep_2017_p(
                        utmp,
                        vtmp,
                        'T', 'ij->n', coeffs)

        # Finally, interpolate to a regular lat/lon grid
        for var in requested_variables:
            lono = lon_flat[inds]
            lato = lat_flat[inds]
            logger.debug('Making interpolator...')
            if var == 'land_binary_mask':
                interpolator = NearestNDInterpolator((lato,
                                                      lono),
                                                      variables[var])
                variables[var] = interpolator(latsm, lonsm)
            else:
                # Special treatment of U/V coordinates if on Cgrid
                if self.grid_type == 'cgrid':
                    if var == 'x_sea_water_velocity':
                        lono = self.lon_U.flatten()[inds]
                        lato = self.lat_U.flatten()[inds]
                    elif var == 'y_sea_water_velocity':
                        lono = self.lon_V.flatten()[inds]
                        lato = self.lat_V.flatten()[inds]
                if variables[var].ndim == 1:
                    interpolator = LinearNDInterpolator((lato,
                                                         lono),
                                                         variables[var])
                    variables[var] = interpolator(latsm, lonsm)
                else:
                    tmp_var = np.empty((len(indz),
                                        latsm.shape[0],
                                        latsm.shape[-1]))
                    for n in range(variables[var].shape[0]):
                        interpolator =\
                            LinearNDInterpolator((lato,
                                                  lono),
                                                 variables[var][n,...])

                        tmp_var[n,...] = interpolator(latsm, lonsm)
                    variables[var] = tmp_var

            variables[var] = np.ma.masked_invalid(variables[var])
        logger.debug('Time for NEMO native reader: ' + str(datetime.now()-start_time))
        return variables


def opa_angle_2016_p(gridfile, orca_grid=False):
    with nc.Dataset(gridfile, 'r') as grid:
        glamt = grid['glamt'][:]
        glamu = grid['glamu'][:]
        glamv = grid['glamv'][:]
        glamf = grid['glamf'][:]

        gphit = grid['gphit'][:]
        gphiu = grid['gphiu'][:]
        gphiv = grid['gphiv'][:]
        gphif = grid['gphif'][:]
    ######################
    # important !!!!!!!!!!!!!!!!
    # to be consistent with old nc library
    ######################
    if glamt.ndim > 2:
        glamt = glamt[0,:,:]
        glamu = glamu[0,:,:]
        glamv = glamv[0,:,:]
        glamf = glamf[0,:,:]

        gphit = gphit[0,:,:]
        gphiu = gphiu[0,:,:]
        gphiv = gphiv[0,:,:]
        gphif = gphif[0,:,:]

    gcost = np.zeros_like(glamt)
    gcosu = np.zeros_like(glamt)
    gcosv = np.zeros_like(glamt)
    gcosf = np.zeros_like(glamt)
    gsint = np.zeros_like(glamt)
    gsinu = np.zeros_like(glamt)
    gsinv = np.zeros_like(glamt)
    gsinf = np.zeros_like(glamt)

    jpj,jpi = np.shape(glamt)

    rpi = np.pi
    rad = np.pi/180.

    for jj in range(1,jpj-1):
        for ji in range(1,jpi):
            # ! north pole direction & modulous (at t-point)
            zlam = glamt[jj,ji]
            zphi = gphit[jj,ji]
            zxnpt = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpt = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpt = zxnpt*zxnpt + zynpt*zynpt

            # ! north pole direction & modulous (at u-point)
            zlam = glamu[jj,ji]
            zphi = gphiu[jj,ji]
            zxnpu = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpu = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpu = zxnpu*zxnpu + zynpu*zynpu

            # ! north pole direction & modulous (at v-point)
            zlam = glamv[jj,ji]
            zphi = gphiv[jj,ji]
            zxnpv = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpv = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpv = zxnpv*zxnpv + zynpv*zynpv

            # ! north pole direction & modulous (at f-point)
            zlam = glamf[jj,ji]
            zphi = gphif[jj,ji]
            zxnpf = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpf = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpf = zxnpf*zxnpf + zynpf*zynpf

            # ! j-direction: v-point segment direction (around t-point)
            zlam = glamv[jj,  ji]
            zphi = gphiv[jj,  ji]
            zlan = glamv[jj-1,ji]
            zphh = gphiv[jj-1,ji]
            zxvvt =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. ) - 2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyvvt =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. ) - 2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znvvt = np.sqrt( znnpt * ( zxvvt*zxvvt + zyvvt*zyvvt )  )
            znvvt = np.array([znvvt, 1.e-14]).max()

            # ! j-direction: f-point segment direction (around u-point)
            zlam = glamf[jj  ,ji]
            zphi = gphif[jj  ,ji]
            zlan = glamf[jj-1,ji]
            zphh = gphif[jj-1,ji]
            zxffu =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyffu =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znffu = np.sqrt( znnpu * ( zxffu*zxffu + zyffu*zyffu )  )
            znffu = np.max(np.array([znffu, 1.e-14]))

            # ! i-direction: f-point segment direction (around v-point)
            zlam = glamf[jj,ji  ]
            zphi = gphif[jj,ji  ]
            zlan = glamf[jj,ji-1]
            zphh = gphif[jj,ji-1]
            zxffv =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyffv =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znffv = np.sqrt( znnpv * ( zxffv*zxffv + zyffv*zyffv )  )
            znffv = np.max(np.array([znffv, 1.e-14]))

            # ! j-direction: u-point segment direction (around f-point)
            zlam = glamu[jj+1,ji]
            zphi = gphiu[jj+1,ji]
            zlan = glamu[jj  ,ji]
            zphh = gphiu[jj  ,ji]
            zxuuf =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyuuf =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znuuf = np.sqrt( znnpf * ( zxuuf*zxuuf + zyuuf*zyuuf )  )
            znuuf = np.max(np.array([znuuf, 1.e-14]))

            # ! cosinus and sinus using scalar and vectorial products
            gsint[jj,ji] = ( zxnpt*zyvvt - zynpt*zxvvt ) / znvvt
            gcost[jj,ji] = ( zxnpt*zxvvt + zynpt*zyvvt ) / znvvt

            gsinu[jj,ji] = ( zxnpu*zyffu - zynpu*zxffu ) / znffu
            gcosu[jj,ji] = ( zxnpu*zxffu + zynpu*zyffu ) / znffu

            gsinf[jj,ji] = ( zxnpf*zyuuf - zynpf*zxuuf ) / znuuf
            gcosf[jj,ji] = ( zxnpf*zxuuf + zynpf*zyuuf ) / znuuf

            # ! (caution, rotation of 90 degres)
            gsinv[jj,ji] = ( zxnpv*zxffv + zynpv*zyffv ) / znffv
            gcosv[jj,ji] =-( zxnpv*zyffv - zynpv*zxffv ) / znffv

    if orca_grid:
        # Treatment of northfold for orca grids
        sign=-1
        gsint = orca_bc(gsint, sign, 'T')
        gcost = orca_bc(gcost, sign, 'T')
        gsinu = orca_bc(gsinu, sign, 'U')
        gcosu = orca_bc(gcosu, sign, 'U')
        gsinv = orca_bc(gsinv, sign, 'V')
        gcosv = orca_bc(gcosv, sign, 'V')
        gsinf = orca_bc(gsinf, sign, 'F')
        gcosf = orca_bc(gcosf, sign, 'F')
    else: 
        # 1:  first row is copied from second row
        gsint[:,0] = gsint[:,1]
        gcost[:,0] = gcost[:,1]
        gsinu[:,0] = gsinu[:,1]
        gcosu[:,0] = gcosu[:,1]
        gsinv[:,0] = gsinv[:,1]
        gcosv[:,0] = gcosv[:,1]
        gsinf[:,0] = gsinf[:,1]
        gcosf[:,0] = gcosf[:,1]

        # 2: last row copied from second last row
        gsint[:,-1] = gsint[:,-2]
        gcost[:,-1] = gcost[:,-2]
        gsinu[:,-1] = gsinu[:,-2]
        gcosu[:,-1] = gcosu[:,-2]
        gsinv[:,-1] = gsinv[:,-2]
        gcosv[:,-1] = gcosv[:,-2]
        gsinf[:,-1] = gsinf[:,-2]
        gcosf[:,-1] = gcosf[:,-2]

        # first column copied from second column
        gsint[0,:] = gsint[1,:]
        gcost[0,:] = gcost[1,:]
        gsinu[0,:] = gsinu[1,:]
        gcosu[0,:] = gcosu[1,:]
        gsinv[0,:] = gsinv[1,:]
        gcosv[0,:] = gcosv[1,:]
        gsinf[0,:] = gsinf[1,:]
        gcosf[0,:] = gcosf[1,:]

        # 4: last column copied from second last column
        gsint[-1,:] = gsint[-2,:]
        gcost[-1,:] = gcost[-2,:]
        gsinu[-1,:] = gsinu[-2,:]
        gcosu[-1,:] = gcosu[-2,:]
        gsinv[-1,:] = gsinv[-2,:]
        gcosv[-1,:] = gcosv[-2,:]
        gsinf[-1,:] = gsinf[-2,:]
        gcosf[-1,:] = gcosf[-2,:]

    return gsint,gcost,gsinu,gcosu,gsinv,gcosv,gsinf,gcosf

    
def rot_rep_2017_p(pxin, pyin, cd_type, cdtodo, coeffs):
    gsint = coeffs[0]; gcost = coeffs[1]
    gsinu = coeffs[2]; gcosu = coeffs[3]
    gsinv = coeffs[4]; gcosv = coeffs[5]
    gsinf = coeffs[6]; gcosf = coeffs[7]

    if cdtodo == 'en->i':
        if cd_type == 'T':
            prot = pxin * gcost + pyin * gsint
        elif cd_type == 'U':
            prot = pxin * gcosu + pyin * gsinu
        elif cd_type == 'V':
            prot = pxin * gcosv + pyin * gsinv
        elif cd_type == 'F':
            prot = pxin * gcosf + pyin * gsinf
    elif cdtodo == 'en->j':
        if cd_type == 'T':
            prot = pyin * gcost - pxin * gsint
        elif cd_type == 'U':
            prot = pyin * gcosu - pxin * gsinu
        elif cd_type == 'V':
            prot = pyin * gcosv - pxin * gsinv
        elif cd_type == 'F':
            prot = pyin * gcosf - pxin * gsinf
    elif cdtodo == 'ij->e':
        if cd_type == 'T':
            prot = pxin * gcost - pyin * gsint
        elif cd_type == 'U':
            prot = pxin * gcosu - pyin * gsinu
        elif cd_type == 'V':
            prot = pxin * gcosv - pyin * gsinv
        elif cd_type == 'F':
            prot = pxin * gcosf - pyin * gsinf
    elif cdtodo == 'ij->n':
        if cd_type == 'T':
            prot = pyin * gcost + pxin * gsint
        elif cd_type == 'U':
            prot = pyin * gcosu + pxin * gsinu
        elif cd_type == 'V':
            prot = pyin * gcosv + pxin * gsinv
        elif cd_type == 'F':
            prot = pyin * gcosf + pxin * gsinf

    return prot

